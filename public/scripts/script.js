var data = [
    { id: 1, pids: [2], gender: 'female', name: "Araesah Hama", photo: 'images/1.jpg', code: '1' },
    { id: 2, pids: [1], gender: 'male', name: "Ibrohim Hama", photo: 'images/0.jpg', code: '10' },
    // รุ่นแรก
    { id: 3, pids: [12], mid: 1, fid: 2, gender: 'female', name: "Yuwareyoh ", photo: 'images/11.jpg', code: '11' },
    { id: 12, pids: [3], gender: 'male', name: "Abdulroshed Bula ", photo: 'images/0.jpg', code: '110' },
    { id: 4, pids: [16], mid: 1, fid: 2, gender: 'female', name: "Patemoh", photo: 'images/0.jpg', code: '12' },
    { id: 16, pids: [4], gender: 'male', name: "Seng Loma", photo: 'images/0.jpg', code: '120' },
    { id: 5, pids: [22], mid: 1, fid: 2, gender: 'male', name: "Thaned", photo: 'images/13.jpg', code: '13' },
    { id: 22, pids: [5], gender: 'female', name: "Hamidah Hama", photo: 'images/130.jpg', code: '130' },
    { id: 6, pids: [25], mid: 1, fid: 2, gender: 'male', name: "Rumlee", photo: 'images/0.jpg', code: '14' },
    { id: 25, pids: [6], gender: 'female', name: "Maedoh Hama", photo: 'images/140.jpg', code: '140' },
    { id: 7, pids: [29], mid: 1, fid: 2, gender: 'male', name: "Abdulloh", photo: 'images/15.jpg', code: '15' },
    { id: 29, pids: [7], gender: 'female', name: "Pattama Hama", photo: 'images/150.jpg', code: '150' },
    { id: 8, pids: [35], mid: 1, fid: 2, gender: 'female', name: "Arseyoh pathan", photo: 'images/16.jpg', code: '16' },
    { id: 35, pids: [8], gender: 'male', name: "smaae pathan", photo: 'images/160.jpg', code: '160' },
    { id: 9, pids: [40], mid: 1, fid: 2, gender: 'female', name: "Rosidah", photo: 'images/17.jpg', code: '17' },
    { id: 40, pids: [9], gender: 'male', name: "Charint", photo: 'images/170.jpg', code: '170' },

    { id: 10, pids: [45], mid: 1, fid: 2, gender: 'male', name: "Asharee", photo: 'images/0.jpg', code: '18' },
    { id: 45, pids: [10], gender: 'female', name: "Suyah", photo: 'images/0.jpg', code: '180' },
    //รุ่น 2
    { id: 11, pids: [49], mid: 3, fid: 12, gender: 'male', name: "Ahamad Bula", photo: 'images/111.jpg', code: '111' },
    { id: 13, pids: [52], mid: 3, fid: 12, gender: 'male', name: "Yahya Bula", photo: 'images/0.jpg', code: '112' },
    { id: 14, pids: [58], mid: 3, fid: 12, gender: 'male', name: "Abdulaziz Bula", photo: 'images/113.jpg', code: '113' },
    { id: 15, pids: [61], mid: 3, fid: 12, gender: 'female', name: "korzimah Bula", photo: 'images/0.jpg', code: '114' },
    //
    { id: 17, pids: [65], mid: 4, fid: 16, gender: 'female', name: "Masenah Loma", photo: 'images/121.jpg', code: '121' },
    { id: 18, pids: [68], mid: 4, fid: 16, gender: 'male', name: "Safeeing Loma", photo: 'images/122.jpg', code: '122' },
    { id: 19, pids: [71], mid: 4, fid: 16, gender: 'female', name: "Veena Loma", photo: 'images/123.jpg', code: '123' },
    { id: 20, pids: [74], mid: 4, fid: 16, gender: 'female', name: "Amena Loma", photo: 'images/124.jpg', code: '124' },
    { id: 21, pids: [81], mid: 4, fid: 16, gender: 'female', name: "Rohanee Loma", photo: 'images/125.jpg', code: '125' },
    //
    { id: 23, mid: 22, fid: 5, gender: 'male', name: "Yusree Hama", photo: 'images/131.jpg', code: '131' },
    { id: 24, pids: [85], mid: 22, fid: 5, gender: 'male', name: "Nusroon Hama", photo: 'images/132.jpg', code: '132' },
    //     
    { id: 26, pids: [86], mid: 25, fid: 6, gender: 'female', name: "Ruwaida Hama", photo: 'images/141.jpg', code: '141' },
    { id: 27, pids: [90], mid: 25, fid: 6, gender: 'male', name: "Amree Hama", photo: 'images/142.jpg', code: '142' },
    { id: 28, mid: 25, fid: 6, gender: 'female', name: "Nureen Hama", photo: 'images/143.jpg', code: '143' },
    //
    { id: 30, pids: [93], mid: 29, fid: 7, gender: 'male', name: "Burhan Hama", photo: 'images/151.jpg', code: '151' },
    { id: 31, pids: [96], mid: 29, fid: 7, gender: 'male', name: "Nuruddin Hama", photo: 'images/152.jpg', code: '152' },
    { id: 32, mid: 29, fid: 7, gender: 'female', name: "Nasneen Hama", photo: 'images/153.jpg', code: '153' },
    { id: 33, mid: 29, fid: 7, gender: 'male', name: "Ruslan Hama", photo: 'images/154.jpg', code: '154' },
    { id: 34, mid: 29, fid: 7, gender: 'male', name: "Hamafee Hama", photo: 'images/155.jpg', code: '155' },
    //
    { id: 36, pids: [98], mid: 8, fid: 35, gender: 'male', name: "Kiston Pathan", photo: 'images/161.jpg', code: '161' },
    { id: 37, mid: 8, fid: 35, gender: 'male', name: "Luzan Pathan", photo: 'images/162.jpg', code: '162' },
    { id: 38, pids: [101], mid: 8, fid: 35, gender: 'female', name: "Naswa Pathan", photo: 'images/163.jpg', code: '163' },
    { id: 39, mid: 8, fid: 35, gender: 'male', name: "Haris Pathan", photo: 'images/164.jpg', code: '164' },
    //
    { id: 41, pids: [103], mid: 9, fid: 40, gender: 'male', name: "Haifah yaena", photo: 'images/0.jpg', code: '171' },
    { id: 42, mid: 9, fid: 40, gender: 'male', name: "Abdulhakim", photo: 'images/0.jpg', code: '172' },
    { id: 43, mid: 9, fid: 40, gender: 'female', name: "Abdulbasir", photo: 'images/173.jpg', code: '173' },
    { id: 44, mid: 9, fid: 40, gender: 'male', name: "Yasmin", photo: 'images/0.jpg', code: '174' },
    //
    { id: 46, mid: 45, fid: 10, gender: 'female', name: "Rohmanee Hama", photo: 'images/181.jpg', code: '181' },
    { id: 47, mid: 45, fid: 10, gender: 'male', name: "Abdulrohman Hama", photo: 'images/182.jpg', code: '182' },
    { id: 48, mid: 45, fid: 10, gender: 'female', name: "Nurwatee Hama", photo: 'images/183.jpg', code: '183' },

    //
    { id: 49, pids: [11], gender: 'female', name: "Mareeene ", photo: 'images/111.jpg', code: '1110' },
    { id: 50, mid: 49, fid: 11, gender: 'male', name: "Abdulkarim Bula", photo: 'images/1111.jpg', code: '1111' },
    { id: 51, mid: 49, fid: 11, gender: 'male', name: "Abdulhakim Bula", photo: 'images/1112.jpg', code: '1112' },
    //
    { id: 52, pids: [13], gender: 'female', name: "", photo: 'images/0.jpg', code: '1120' },
    { id: 53, pids: [106], mid: 52, fid: 13, gender: 'male', name: "Arisman Bula", photo: 'images/0.jpg', code: '1121' },
    { id: 54, mid: 52, fid: 13, gender: 'male', name: "Hadee Bula ", photo: 'images/0.jpg', code: '1122' },
    { id: 55, mid: 52, fid: 13, gender: 'male', name: " ", photo: 'images/1123.jpg', code: '1123' },
    { id: 56, mid: 52, fid: 13, gender: 'male', name: "Kasmee Bula ", photo: 'images/1124.jpg', code: '1124' },
    { id: 57, mid: 52, fid: 13, gender: 'male', name: " ", photo: 'images/1125.jpg', code: '1125' },
    //
    { id: 58, pids: [14], gender: 'female', name: "Zainab Bula", photo: 'images/1130.jpg', code: '1130' },
    { id: 59, mid: 58, fid: 14, gender: 'male', name: "Akrom Bula", photo: 'images/1131.jpg', code: '1131' },
    { id: 60, mid: 58, fid: 14, gender: 'male', name: "Kairunanwar Bula", photo: 'images/1132.jpg', code: '1132' },
    //
    { id: 61, pids: [15], gender: 'male', name: "Ibrahim Mae", photo: 'images/1140.jpg', code: '1140' },
    { id: 62, mid: 15, fid: 61, gender: 'male', name: "Awee", photo: 'images/1141.jpg', code: '1141' },
    { id: 63, mid: 15, fid: 61, gender: 'male', name: "Alamg", photo: 'images/1142.jpg', code: '1142' },
    { id: 64, mid: 15, fid: 61, gender: 'female', name: "Anis", photo: 'images/1143.jpg', code: '1143' },
    //
    { id: 65, pids: [17], gender: 'male', name: "Ahmad chero", photo: 'images/0.jpg', code: '1210' },
    { id: 66, mid: 17, fid: 65, gender: 'female', name: "Iffar Chero", photo: 'images/0.jpg', code: '1211' },
    { id: 67, mid: 17, fid: 65, gender: 'male', name: "Alaml Chero", photo: 'images/0.jpg', code: '1212' },
    //
    { id: 68, pids: [18], gender: 'female', name: "Latepah loma", photo: 'images/1220.jpg', code: '1220' },
    { id: 69, mid: 18, fid: 68, gender: 'male', name: "Ahamdhafifi Loma", photo: 'images/1221.jpg', code: '1221' },
    { id: 70, mid: 18, fid: 68, gender: 'male', name: "Usman Loma", photo: 'images/1222.jpg', code: '1222' },
    //
    { id: 71, pids: [19], gender: 'male', name: " ", photo: 'images/1230.jpg', code: '1230' },
    { id: 72, mid: 19, fid: 71, gender: 'female', name: "Nurmin said", photo: 'images/123.jpg', code: '1231' },
    { id: 73, mid: 19, fid: 71, gender: 'female', name: "Kausar Said", photo: 'images/123.jpg', code: '1232' },
    //
    { id: 74, pids: [20], gender: 'male', name: "Ismail  Kuetu", photo: 'images/1240.jpg', code: '1240' },
    { id: 75, mid: 20, fid: 74, gender: 'female', name: "Zainab Kuetu", photo: 'images/1241.jpg', code: '1241' },
    { id: 76, mid: 20, fid: 74, gender: 'female', name: "", photo: 'images/12420.jpg', code: '1242' },
    { id: 77, mid: 20, fid: 74, gender: 'female', name: "Najmee Kuetu", photo: 'images/1243.jpg', code: '1243' },
    { id: 78, mid: 20, fid: 74, gender: 'male', name: "Sorla Ketu", photo: 'images/1244.jpg', code: '1244' },
    { id: 79, mid: 20, fid: 74, gender: 'female', name: "Kairun Kuetu", photo: 'images/1245.jpg', code: '1245' },
    { id: 80, mid: 20, fid: 74, gender: 'male', name: "Asrof Kuetu", photo: 'images/1246.jpg', code: '1246' },
    //
    { id: 81, pids: [21], gender: 'male', name: "Syukron Abdulkadir", photo: 'images/1250.jpg', code: '1250' },
    { id: 82, mid: 21, fid: 81, gender: 'male', name: "Hamza Abdulkadir", photo: 'images/0.jpg', code: '1251' },
    { id: 83, mid: 21, fid: 81, gender: 'male', name: "Korlid Abdulkadir", photo: 'images/0.jpg', code: '1252' },
    { id: 84, mid: 21, fid: 81, gender: 'male', name: "Iyub Abdulkadir", photo: 'images/0.jpg', code: '1253' },
    //
    { id: 85, pids: [24], gender: 'female', name: "Habebah Hama", photo: 'images/1320.jpg', code: '1320' },
    // 
    { id: 86, pids: [26], gender: 'male', name: "Zulkiflee Saleh", photo: 'images/1410.jpg', code: '1410' },
    { id: 87, mid: 26, fid: 86, gender: 'male', name: "Azan Saleh", photo: 'images/1411.jpg', code: '1411' },
    { id: 88, mid: 26, fid: 86, gender: 'male', name: "Amir Saleh", photo: 'images/1412.jpg', code: '1412' },
    { id: 89, mid: 26, fid: 86, gender: 'female', name: "Amina Saleh ", photo: 'images/1413.jpg', code: '1413' },
    //
    { id: 90, pids: [27], gender: 'female', name: "Aisiyakorn Kayankarn", photo: 'images/1420.jpg', code: '1420' },
    { id: 91, mid: 90, fid: 27, gender: 'female', name: "Aswanee Hama", photo: 'images/0.jpg', code: '1421' },
    { id: 92, mid: 90, fid: 27, gender: 'male', name: "Shavee Hama", photo: 'images/0.jpg', code: '1422' },
    //
    { id: 93, pids: [30], gender: 'female', name: "Aisha Hama", photo: 'images/1510.jpg', code: '1510' },
    { id: 94, mid: 93, fid: 30, gender: 'male', name: "Muklis Hama", photo: 'images/1511.jpg', code: '1511' },
    { id: 95, mid: 93, fid: 30, gender: 'male', name: "Shakir Hama", photo: 'images/1512.jpg', code: '1512' },
    //
    { id: 96, pids: [31], gender: 'female', name: "Husna Hama", photo: 'images/1520.jpg', code: '1520' },
    { id: 97, mid: 96, fid: 31, gender: 'female', name: "Shereen Hama", photo: 'images/1521.jpg', code: '1521' },
    //
    { id: 98, pids: [36], gender: 'female', name: "Niwildan Pathan", photo: 'images/1610.jpg', code: '1610' },
    { id: 99, mid: 98, fid: 36, gender: 'female', name: "Niamilia Pathan", photo: 'images/1611.jpg', code: '1611' },
    { id: 100, mid: 98, fid: 36, gender: 'female', name: "Niamalina Pathan", photo: 'images/1612.jpg', code: '1612' },
    //
    { id: 101, pids: [38], gender: 'male', name: "Imron Benhawan", photo: 'images/1630.jpg', code: '1630' },
    { id: 102, mid: 38, fid: 101, gender: 'female', name: "Reeham Benhawan", photo: 'images/1631.jpg', code: '1631' },
    //
    { id: 103, pids: [41], gender: 'male', name: "Anwar ", photo: 'images/0.jpg', code: '1710' },
    { id: 104, mid: 41, fid: 103, gender: 'male', name: "Marwan ", photo: 'images/0.jpg', code: '1711' },
    { id: 105, mid: 41, fid: 103, gender: 'male', name: " ", photo: 'images/0.jpg', code: '1712' },
    //
    { id: 106, pids: [53], gender: 'female', name: "young", photo: 'images/0.jpg', code: '11210' },
    { id: 107, mid: 106, fid: 53, gender: 'female', name: "Hanefah ", photo: 'images/0.jpg', code: '11211' },
    { id: 108, mid: 106, fid: 53, gender: 'male', name: "abah ", photo: 'images/0.jpg', code: '11211' },


]
FamilyTree.templates.tommy_male.plus =
    '<circle cx="0" cy="0" r="15" fill="#ffffff" stroke="#aeaeae" stroke-width="1"></circle>'
    + '<line x1="-11" y1="0" x2="11" y2="0" stroke-width="1" stroke="#aeaeae"></line>'
    + '<line x1="0" y1="-11" x2="0" y2="11" stroke-width="1" stroke="#aeaeae"></line>';
FamilyTree.templates.tommy_male.minus =
    '<circle cx="0" cy="0" r="15" fill="#ffffff" stroke="#aeaeae" stroke-width="1"></circle>'
    + '<line x1="-11" y1="0" x2="11" y2="0" stroke-width="1" stroke="#aeaeae"></line>';
FamilyTree.templates.tommy_female.plus =
    '<circle cx="0" cy="0" r="15" fill="#ffffff" stroke="#aeaeae" stroke-width="1"></circle>'
    + '<line x1="-11" y1="0" x2="11" y2="0" stroke-width="1" stroke="#aeaeae"></line>'
    + '<line x1="0" y1="-11" x2="0" y2="11" stroke-width="1" stroke="#aeaeae"></line>';
FamilyTree.templates.tommy_female.minus =
    '<circle cx="0" cy="0" r="15" fill="#ffffff" stroke="#aeaeae" stroke-width="1"></circle>'
    + '<line x1="-11" y1="0" x2="11" y2="0" stroke-width="1" stroke="#aeaeae"></line>';

FamilyTree.templates.tommy_female.defs = '<g transform="matrix(0.05,0,0,0.05,-12,-9)" id="heart"><path fill="#F57C00" d="M438.482,58.61c-24.7-26.549-59.311-41.655-95.573-41.711c-36.291,0.042-70.938,15.14-95.676,41.694l-8.431,8.909  l-8.431-8.909C181.284,5.762,98.663,2.728,45.832,51.815c-2.341,2.176-4.602,4.436-6.778,6.778 c-52.072,56.166-52.072,142.968,0,199.134l187.358,197.581c6.482,6.843,17.284,7.136,24.127,0.654 c0.224-0.212,0.442-0.43,0.654-0.654l187.29-197.581C490.551,201.567,490.551,114.77,438.482,58.61z"/><g>';
FamilyTree.templates.tommy_male.defs = '<g transform="matrix(0.05,0,0,0.05,-12,-9)" id="heart"><path fill="#F57C00" d="M438.482,58.61c-24.7-26.549-59.311-41.655-95.573-41.711c-36.291,0.042-70.938,15.14-95.676,41.694l-8.431,8.909  l-8.431-8.909C181.284,5.762,98.663,2.728,45.832,51.815c-2.341,2.176-4.602,4.436-6.778,6.778 c-52.072,56.166-52.072,142.968,0,199.134l187.358,197.581c6.482,6.843,17.284,7.136,24.127,0.654 c0.224-0.212,0.442-0.43,0.654-0.654l187.29-197.581C490.551,201.567,490.551,114.77,438.482,58.61z"/><g>';


var family = new FamilyTree(document.getElementById("tree"), {
    mode: 'dark',
    template: "tommy",
    // mouseScrool: FamilyTree.action.none,
    // scaleInitial: FamilyTree.match.width,
    showXScroll: FamilyTree.scroll.visible,
    showYScroll: FamilyTree.scroll.visible,
    mouseScrool: FamilyTree.action.zoom,
    enableSearch: false,
    roots: [1],
    nodeTreeMenu: true,
    nodeMenu: {
        // edit: { text: 'Edit' },
        details: { text: 'Details' },
    }, editForm: {
        titleBinding: "name",
        photoBinding: "photo",
        readOnly: true,
    },
    nodeBinding: {
        field_0: "name",
        field_1: 'code',
        // field_2: 'code',
        img_0: 'photo'
    },
    // nodes:data
});
// family.on('field', function (sender, args) {
//     if (args.name == 'code') {
//         var date = new Date(args.value);
//         args.value = date.toLocaleDateString();
//     }
// });
family.on('expcollclick', function (sender, isCollapsing, nodeId) {
    var node = family.getNode(nodeId);
    if (isCollapsing) {
        family.expandCollapse(nodeId, [], node.ftChildrenIds)
    }
    else {
        family.expandCollapse(nodeId, node.ftChildrenIds, [])
    }
    return false;
});
family.on('render-link', function (sender, args) {
    if (args.cnode.ppid != undefined)
        args.html += '<use data-ctrl-ec-id="' + args.node.id + '" xlink:href="#heart" x="' + (args.p.xb) + '" y="' + (args.p.ya) + '"/>';
    if (args.cnode.isPartner && args.node.partnerSeparation == 30)
        args.html += '<use data-ctrl-ec-id="' + args.node.id + '" xlink:href="#heart" x="' + (args.p.xb) + '" y="' + (args.p.yb) + '"/>';

});
family.load(data)